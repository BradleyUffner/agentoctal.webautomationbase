﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AgentOctal.WebAutomationBase.Extensions
{
    using System.Linq.Expressions;
    using System.Reflection;

    /// <summary>
    /// Contains methods extending 'object'
    /// </summary>
    public static class ObjectExtensions
    {
        /// <summary>
        /// Gets the PropertyInfo from an Expression.
        /// </summary>
        /// <typeparam name="TSource">The Type of the source object</typeparam>
        /// <typeparam name="TProperty">The Type of the property</typeparam>
        /// <param name="source">The object containing the property</param>
        /// <param name="propertyLambda">The expression containing the property</param>
        /// <returns>The PropertyInfo referenced by the propertyLambda argument</returns>
        /// <example>
        /// <code>
        /// someVariable.GetPropertyInfo(source =&gt; source.SomeProperty);
        /// </code>
        /// </example>
        public static PropertyInfo GetPropertyInfo<TSource, TProperty>(
            this TSource source,
            Expression<Func<TSource, TProperty>> propertyLambda)
        {
            var type = typeof(TSource);
            var member = propertyLambda.Body as MemberExpression;
            if (member == null)
            {
                throw new ArgumentException($"Expression '{propertyLambda.ToString()}' refers to a method, not a property.");
            }

            var propInfo = member.Member as PropertyInfo;
            if (propInfo == null)
            {
                throw new ArgumentException($"Expression '{propertyLambda.ToString()}' refers to a field, not a property.");
            }

            if (type != propInfo.ReflectedType &&
                !type.IsSubclassOf(propInfo.ReflectedType))
            {
                throw new ArgumentException($"Expression '{propertyLambda.ToString()}' refers to a property that is not from type {type}.");
            }

            return propInfo;
        }
    }
}