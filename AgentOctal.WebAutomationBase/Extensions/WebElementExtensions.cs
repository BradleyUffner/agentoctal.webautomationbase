﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;

namespace AgentOctal.WebAutomationBase.Extensions
{
    public static class WebElementExtensions
    {
        /// <summary>
        /// This method provides a shorthand for capturing the parent of a selected element.
        /// </summary>
        /// <param name="element">Current element</param>
        /// <returns>Current element's parent</returns>
        public static IWebElement GetParent(this IWebElement element)
        {
            return element.FindElement(By.XPath(".."));
        }

        public static void EnterText(this IWebElement element, string inputText)
        {
            element.Click();
            element.SendKeys(inputText);
        }
    }
}
