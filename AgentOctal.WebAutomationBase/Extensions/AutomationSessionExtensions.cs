﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using SeleniumExtras.WaitHelpers;

namespace AgentOctal.WebAutomationBase.Extensions
{
    public static class AutomationSessionExtensions
    {
        /// <summary>
        /// Tries to find an element by a locator. This function does NOT wait.  It will throw an error if not found.
        /// </summary>
        /// <param name="session">The IAutomationSession to use</param>
        /// <param name="locator">The locating strategy for the IWebElement</param>
        /// <returns>The IWebElement found by locator</returns>
        public static IWebElement FindElement(this IAutomationSession session, By locator)
        {
            session.Log($"Searching for: {locator}");
            return session.Driver.FindElement(locator);
        }


        /// <summary>
        /// Simulates a click on the IWebElement
        /// </summary>
        /// <param name="session">The IAutomationSession to use</param>
        /// <param name="locator">The locating strategy for the IWebElement</param>
        public static void Click(this IAutomationSession session, By locator) =>
            session.Driver.FindElement(locator).Click();

        /// <summary>
        /// Waits for the specified condition to be true.
        /// </summary>
        /// <typeparam name="TResult">The return type of the operation</typeparam>
        /// <param name="session">The IAutomationSession to use</param>
        /// <param name="condition">The condition to wait for.</param>
        /// <returns>The reference returned by <paramref name="condition"/></returns>
        public static TResult WaitUntil<TResult>(this IAutomationSession session, Func<IWebDriver, TResult> condition) =>
            WaitUntil(session, condition, "Unspecified event", session.Options.DefaultWaitTimeout);

        /// <summary>
        /// Waits for the specified condition to be true.
        /// </summary>
        /// <typeparam name="TResult">The return type of the operation</typeparam>
        /// <param name="session">The IAutomationSession to use</param>
        /// <param name="condition">The condition to wait for.</param>
        /// <param name="timeout">How long to wait</param>
        /// <returns>The reference returned by <paramref name="condition"/></returns>
        public static TResult WaitUntil<TResult>(this IAutomationSession session, Func<IWebDriver, TResult> condition, string @for, TimeSpan timeout)
        {
            session.OnWaiting(@for, timeout);
            var result = new WebDriverWait(session.Driver, timeout).Until(condition);
            session.OnWaitCompleted();
            return result;
        }


        /// <summary>
        /// Waits for an element to exist and be visible.
        /// </summary>
        /// <param name="session">The IAutomationSession to use</param>
        /// <param name="locator">The locating strategy for the IWebElement</param>
        /// <returns>The IWebElement located by <paramref name="locator"/></returns>
        public static IWebElement WaitUntilElementVisible(this IAutomationSession session, By locator) =>
            WaitUntilElementVisible(session, locator, session.Options.DefaultWaitTimeout);

        /// <summary>
        /// Waits for an element to exist and be visible.
        /// </summary>
        /// <param name="session">The IAutomationSession to use</param>
        /// <param name="locator">The locating strategy for the IWebElement</param>
        /// <param name="timeout">How long to wait</param>
        /// <returns>The IWebElement located by <paramref name="locator"/></returns>
        public static IWebElement WaitUntilElementVisible(this IAutomationSession session, By locator, TimeSpan timeout)
        {
            session.Log($"Waiting for `{locator}` to become visible");
            return WaitUntil(session, ExpectedConditions.ElementIsVisible(locator), $"Element visible: {locator}", timeout);
        }

        /// <summary>
        /// Waits for an element to exist and be visible.
        /// </summary>
        /// <param name="session">The IAutomationSession to use</param>
        /// <param name="locator">The locating strategy for the IWebElement</param>
        /// <returns>The IWebElement located by <paramref name="locator"/></returns>
        public static IWebElement WaitUntilElementExists(this IAutomationSession session, By locator) =>
            WaitUntilElementExists(session, locator, session.Options.DefaultWaitTimeout);

        /// <summary>
        /// Waits for an element to exist and be visible.
        /// </summary>
        /// <param name="session">The IAutomationSession to use</param>
        /// <param name="locator">The locating strategy for the IWebElement</param>
        /// <param name="timeout">How long to wait</param>
        /// <returns>The IWebElement located by <paramref name="locator"/></returns>
        public static IWebElement WaitUntilElementExists(this IAutomationSession session, By locator, TimeSpan timeout)
        {
            session.Log($"Waiting for `{locator}` to exist");
            return WaitUntil(session, ExpectedConditions.ElementExists(locator), $"Element exists: {locator}", timeout);
        }

        /// <summary>
        /// Waits for an element to exist and be visible.
        /// </summary>
        /// <param name="session">The IAutomationSession to use</param>
        /// <param name="locator">The locating strategy for the IWebElement</param>
        /// <returns>A boolean indicating if the element is visible</returns>
        public static bool WaitUntilElementNotVisible(this IAutomationSession session, By locator) =>
            WaitUntilElementNotVisible(session, locator, session.Options.DefaultWaitTimeout);

        /// <summary>
        /// Waits for an element to exist and be visible.
        /// </summary>
        /// <param name="session">The IAutomationSession to use</param>
        /// <param name="locator">The locating strategy for the IWebElement</param>
        /// <param name="timeout">How long to wait</param>
        /// <returns>A boolean indicating if the element is visible</returns>
        public static bool WaitUntilElementNotVisible(this IAutomationSession session, By locator, TimeSpan timeout)
        {
            session.Log($"Waiting for `{locator}` to go away");
            return WaitUntil(session, ExpectedConditions.InvisibilityOfElementLocated(locator), $"Element not visible: {locator}", timeout);
        }

        /// <summary>
        /// Waits for an element to exist and be visible.
        /// </summary>
        /// <param name="session">The IAutomationSession to use</param>
        /// <param name="locator">The locating strategy for the IWebElement</param>
        /// <param name="text">The text to search for</param>
        /// <returns>A boolean indicating if the element contains teh specified text</returns>
        public static bool WaitUntilElementContainsText(this IAutomationSession session, By locator, string text) =>
            WaitUntilElementContainsText(session, locator, text, session.Options.DefaultWaitTimeout);

        /// <summary>
        /// Waits for an element to exist, be visible.
        /// </summary>
        /// <param name="session">The IAutomationSession to use</param>
        /// <param name="locator">The locating strategy for the IWebElement</param>
        /// <param name="text">The text to search for</param>
        /// <param name="timeout">How long to wait</param>
        /// <returns>A boolean indicating if the element contains teh specified text<</returns>
        public static bool WaitUntilElementContainsText(this IAutomationSession session, By locator, string text, TimeSpan timeout)
        {
            session.Log($"Waiting for `{locator}` to contain `{text}`");
            return WaitUntil(session, ExpectedConditions.TextToBePresentInElementLocated(locator, text), $"Element to contain text: {locator}", timeout);
        }
    }
}
