﻿using System;

namespace AgentOctal.WebAutomationBase.Logging
{
    public abstract class LoggingEvent : EventArgs
    {
        public LoggingEvent(LogLevel logLevel)
        {
            LogLevel = logLevel;
        }

        public LogLevel LogLevel { get; }
    }
}
