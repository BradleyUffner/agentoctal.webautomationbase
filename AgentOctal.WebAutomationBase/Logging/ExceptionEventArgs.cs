﻿using System;

namespace AgentOctal.WebAutomationBase.Logging
{
    public class ExceptionEventArgs : LoggingEvent
    {
        public ExceptionEventArgs(Exception ex, LogLevel logLevel) : base(logLevel)
        {
            Ex = ex;
        }

        public Exception Ex { get; set; }
    }
}
