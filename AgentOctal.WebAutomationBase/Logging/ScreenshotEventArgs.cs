﻿using System.Drawing;

namespace AgentOctal.WebAutomationBase.Logging
{
    public class ScreenshotEventArgs : LoggingEvent
    {
        public ScreenshotEventArgs(Bitmap image) : this(image, "", LogLevel.Normal) { }

        public ScreenshotEventArgs(Bitmap image, string message) : this(image, message, LogLevel.Normal) { }

        public ScreenshotEventArgs(Bitmap image, string message, LogLevel logLevel) :base(logLevel)
        {
            Image = image;
            Message = message;
        }

        public Bitmap Image { get; }

        public string Message { get; }
    }
}
