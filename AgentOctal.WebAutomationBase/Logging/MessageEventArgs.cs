﻿namespace AgentOctal.WebAutomationBase.Logging
{
    public class MessageEventArgs :  LoggingEvent
    {
        public MessageEventArgs(string message):this(message, LogLevel.Normal) { }

        public MessageEventArgs(string message, LogLevel logLevel) : base(logLevel)
        {
            Message = message;
        }

        public string Message { get; set; }
    }
}
