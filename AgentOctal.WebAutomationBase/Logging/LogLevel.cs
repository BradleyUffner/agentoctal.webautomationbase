﻿namespace AgentOctal.WebAutomationBase.Logging
{
    public enum LogLevel
    {
        Low,
        Normal,
        High,
    }
}
