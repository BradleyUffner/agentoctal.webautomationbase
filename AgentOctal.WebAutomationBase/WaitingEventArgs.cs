﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AgentOctal.WebAutomationBase
{
    public class WaitingEventArgs:EventArgs
    {
        public WaitingEventArgs(string @for, TimeSpan timeout)
        {
            For = @for;
            Timeout = timeout;
        }

        public string For { get; }
        public TimeSpan Timeout { get; }
    }
}
