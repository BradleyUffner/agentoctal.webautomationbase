﻿namespace AgentOctal.WebAutomationBase.Navigation
{
    public abstract class Area
    {
        public Area(string path)
        {
            Path = path;

            AreaManager.RegisterArea(this);
        }

        public string Path { get; }

        public abstract string ApplicationIdentifier { get; }
        
        public void Navigate(IAutomationSession session)
        {
            session.NavigateTo(this);
        }
    }
}
