﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AgentOctal.WebAutomationBase.Navigation
{
    public class UriArea : Area
    {
        public UriArea(string path) : base(path) { }

        public override string ApplicationIdentifier => "";
    }
}
