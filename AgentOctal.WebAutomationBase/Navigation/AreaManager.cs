﻿using System.Collections.Generic;
using System.Linq;

namespace AgentOctal.WebAutomationBase.Navigation
{
    public class AreaManager
    {
        private static HashSet<Area> _areas = new HashSet<Area>();

        public static void RegisterArea(Area area)
        {
            _areas.Add(area);
        }

        public static IEnumerable<Area> Areas => _areas;

        public IEnumerable<TArea> GetAreas<TArea>() where TArea : Area
        {
            return Areas.OfType<TArea>();
        }
    }
}
