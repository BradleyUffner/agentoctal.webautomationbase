﻿namespace AgentOctal.WebAutomationBase.Navigation
{
    public interface IUrlBuilder {
        string GetUrlFor(IAutomationSession session, Area area);
        string GetFullHost(IAutomationSession session);
    }

    public abstract class UrlBuilder<TAutomationOptions> : IUrlBuilder where TAutomationOptions : AutomationOptions
    {
        public abstract string GetUrlFor(AutomationSession<TAutomationOptions> session, Area area);

        public virtual string GetFullHost(AutomationSession<TAutomationOptions> session)
        {
            if (session.Options.Environment.Port == 80 && session.Options.Host.StartsWith("http://") ||
                session.Options.Environment.Port == 443 && session.Options.Host.StartsWith("https://"))
            {
                return session.Options.Host;
            }

            return $"{session.Options.Host}:{session.Options.Environment.Port}";
        }

        string IUrlBuilder.GetUrlFor(IAutomationSession session, Area area)
        {
            return GetUrlFor((AutomationSession<TAutomationOptions>)session, area);
        }

        string IUrlBuilder.GetFullHost(IAutomationSession session)
        {
            return GetFullHost((AutomationSession<TAutomationOptions>) session);
        }
    }
}
