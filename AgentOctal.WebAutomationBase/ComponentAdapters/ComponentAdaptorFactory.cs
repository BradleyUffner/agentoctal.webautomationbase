﻿using OpenQA.Selenium;

namespace AgentOctal.WebAutomationBase.ComponentAdapters
{
    using System;
    using System.Linq.Expressions;
    using Extensions;
    using InputControlAdapters;

    /// <summary>
    /// Contains methods for building ComponentAdapters
    /// </summary>
    public static class ComponentAdapterFactory
    {
        /// <summary>
        /// Creates and populates a ComponentAdapter from a DataModel
        /// </summary>
        /// <typeparam name="TAdapter">The Type of the adapter to create</typeparam>
        /// <param name="dataModel">The data model to use as a source of data</param>
        /// <param name="session">The IautomationSession to associate with this adapter</param>
        /// <returns>A ComponentAdapter</returns>
        public static TAdapter CreateFromModel<TAdapter>(ComponentAdapterModel dataModel, IAutomationSession session)
            where TAdapter : IComponentAdapter
        {
            var adapter = CreateSessionAssociatedAdapterInstance<TAdapter>(session);

            adapter.PopulateFromDataModel(dataModel);

            return adapter;
        }

        ///// <summary>
        ///// Creates a ComponentAdapter
        ///// </summary>
        ///// <typeparam name="TAdapter">The Type of the adapter to create</typeparam>
        ///// <returns>A ComponentAdapter</returns>
        //public static TAdapter Create<TAdapter>()
        //    where TAdapter : IComponentAdapter, new()
        //{
        //    var adapter = new TAdapter();

        //    return adapter;
        //}

        /// <summary>
        /// Creates a ComponentAdapter
        /// </summary>
        /// <typeparam name="TAdapter">The Type of the adapter to create</typeparam>
        /// <param name="session">The IautomationSession to associate with this adapter</param>
        /// <returns>A ComponentAdapter</returns>
        public static TAdapter Create<TAdapter>(IAutomationSession session)
            where TAdapter : IComponentAdapter
        {
            var adapter = CreateSessionAssociatedAdapterInstance<TAdapter>(session);

            return adapter;
        }

        /// <summary>
        /// Sets a property of the ComponentAdapter
        /// </summary>
        /// <typeparam name="TAdapter">The type of the adapter</typeparam>
        /// <typeparam name="TValue">The type of the value to set</typeparam>
        /// <param name="adapter">The ComponentAdapter to set the value on</param>
        /// <param name="property">An Expression contianing the property of the ComponentAdapter to set</param>
        /// <param name="value">The value to set</param>
        /// <returns>The ComponentAdapter instance this method was called on, suitable for method chaining</returns>
        public static TAdapter SetProperty<TAdapter, TValue>(this TAdapter adapter,
                                                             Expression<Func<TAdapter, InputControlAdapter<TValue>>> property,
                                                             TValue value)
            where TAdapter : IComponentAdapter
        {
            var adapterProp = adapter.GetPropertyInfo(property);
            var adapterPropVal = adapterProp.GetValue(adapter);
            var valueProp = adapterPropVal.GetType().GetProperty("Value");
            valueProp?.SetValue(adapterPropVal, value);

            return adapter;
        }

        /// <summary>
        /// Creates an instance of the specified ComponentAdapter associated with the specified AutomationSession
        /// </summary>
        /// <typeparam name="TAdapter"></typeparam>
        /// <param name="session"></param>
        /// <returns></returns>
        private static TAdapter CreateSessionAssociatedAdapterInstance<TAdapter>(IAutomationSession session)
        {
            if (session == null)
            {
                throw new ArgumentException("session can not be null", nameof(session));
            }
            TAdapter adapter = (TAdapter)Activator.CreateInstance(typeof(TAdapter), session);
            return adapter;
        }
    }
}
