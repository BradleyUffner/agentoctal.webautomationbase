﻿using AgentOctal.WebAutomationBase.Extensions;

namespace AgentOctal.WebAutomationBase.ComponentAdapters
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Threading;
    using InputControlAdapters;
    using OpenQA.Selenium;

    /// <summary>
    /// Exposes a field of a web page to automation
    /// </summary>
    /// <typeparam name="TItem">The Type of value supported by the adapter</typeparam>
    public abstract class ComponentAdapter<TItem> :
            IComponentAdapter
            where TItem : ComponentAdapterModel
    {

        /// <summary>
        /// Creates a new instance associated with an AutomationSession
        /// </summary>
        /// <param name="session"></param>
        public ComponentAdapter(IAutomationSession session)
        {
            Session = session;
        }

        /// <summary>
        /// The AutomationSession associated with this ComponentAdapter
        /// </summary>
        public IAutomationSession Session { get; }

        /// <summary>
        /// Returns the name of a ControlAdapter on this ComponentAdapter
        /// </summary>
        /// <param name="inputControlAdapter">The instance of the ControlAdapter
        /// on this ComponentAdapter to get the name of.</param>
        /// <returns>A string containing the name of the control adapter</returns>
        public string GetNameOf(IInputControlAdapter inputControlAdapter)
        {
            return GetControlAdapterInfo()
                   .FirstOrDefault(adapter => adapter.Value == inputControlAdapter)
                   .Key;
        }

        /// <summary>
        /// Moves data from the dataModel to the ComponentAdapter
        /// </summary>
        /// <param name="dataModel">The ComponentAdapterModel to use as a data source</param>
        protected virtual void PopulateFromDataModel(TItem dataModel) { }

        /// <summary>
        /// Moves data from the dataModel to the ComponentAdapter
        /// </summary>
        /// <param name="dataModel">The ComponentAdapterModel to use as a data source</param>
        void IComponentAdapter.PopulateFromDataModel(object dataModel)
        {
            PopulateFromDataModel((TItem)dataModel);
        }

        /// <summary>
        /// Asks ControlAdapter properties on this ComponentAdapter to fill in their fields on the website.
        /// </summary>
        /// <param name="session">TheAutomationSession</param>
        public void PopulateFields(IAutomationSession session)
        {
            var fieldErrors = new List<string>();

            foreach (var prop in this.GetType()
                                     .GetProperties(BindingFlags.Instance | BindingFlags.NonPublic))
            {
                var field = prop.GetValue(this) as IInputControlAdapter;
                if (field?.Value != null)
                {
                    try
                    {
                        field.PopulateControl(session);
                    }
                    catch (Exception ex)
                    {
                        throw new InvalidOperationException($"Error populating field '{prop.Name}' on '{this.GetType().Name}'", ex);
                    }
                }
            }

            if (fieldErrors.Any())
            {
                throw new ElementNotInteractableException($"Could not enter values for: {string.Join(", ", fieldErrors)}");
            }
        }

        /// <summary>
        /// Populates the fields on the adapter from the field values of a datasource with matching names.
        /// </summary>
        /// <param name="datagram"></param>
        public void PopulateFromDatagram(Dictionary<string, string> datagram)
        {
            foreach (var controlAdapter in GetControlAdapterInfo().Values)
            {
                if (datagram.TryGetValue(controlAdapter.GetName(), out var value))
                {
                    controlAdapter.Value = value;
                    controlAdapter.PopulateControl(Session);
                }
            }


            //foreach (var prop in GetType()
            //    .GetProperties(BindingFlags.Instance |
            //                   BindingFlags.NonPublic |
            //                   BindingFlags.Public))
            //{
            //    if (prop.GetValue(this) is IInputControlAdapter controlAdapter &&
            //        datagram.TryGetValue(prop.Name, out var value))
            //    {
            //        controlAdapter.Value = value;
            //        controlAdapter.PopulateControl(Session);
            //    }
            //}
        }

        private Dictionary<string, IInputControlAdapter> _controlAdapterInfo;

        public Dictionary<string, IInputControlAdapter> GetControlAdapterInfo()
        {
            if (_controlAdapterInfo != null)
            {
                return _controlAdapterInfo;
            }

            var info = new Dictionary<string, IInputControlAdapter>();

            foreach (var prop in GetType()
                .GetProperties(BindingFlags.Instance |
                               BindingFlags.NonPublic |
                               BindingFlags.Public))
            {
                if (prop.GetValue(this) is IInputControlAdapter controlAdapter)
                {
                    info.Add(prop.Name, controlAdapter);
                }
            }

            _controlAdapterInfo = info;
            return info;
        }

        /// <summary>
        /// Gets the By used to locate an HTML element on the page that contains the ComponentAdapter.
        /// </summary>
        protected abstract By Locator { get; }

        /// <summary>
        /// Checks to see if the HTML content this adapter adapts is present on the page
        /// </summary>
        /// <param name="session">The AutomationSession</param>
        /// <returns>'true' if the adapter's HTML content is available on the page</returns>
        public bool IsComponentPresent(IAutomationSession session)
        {

            try
            {
                return session.FindElement(Locator) != null;
            }
            catch (NoSuchElementException)
            {
                return false;
            }
        }

        /// <summary>
        /// Sleeps the current Thread for a period of time
        /// </summary>
        /// <param name="milliseconds">The number of milliseconds to sleep for</param>
        public static void Sleep(int milliseconds) => Thread.Sleep(milliseconds);

        /// <summary>
        /// Sleeps the current Thread for a period of time
        /// </summary>
        /// <param name="sleeptime">The TimeSpan to sleep for</param>
        public static void Sleep(TimeSpan sleeptime) => Sleep((int)sleeptime.TotalMilliseconds);

        /// <summary>
        /// Waits for the component to be fully loaded.
        /// </summary>
        /// <remarks>
        /// The base implementation waits for the Locator element to become visible.
        /// This behavior can be changed by overriding.
        /// </remarks>
        public virtual void WaitUntilLoaded()
        {
            Session.WaitUntilElementVisible(Locator);
        }
    }
}
