﻿using System.Collections.Generic;
using AgentOctal.WebAutomationBase.InputControlAdapters;

namespace AgentOctal.WebAutomationBase.ComponentAdapters
{
    using OpenQA.Selenium;

    /// <summary>
    /// A non-generic interface representing a ComponentAdapter
    /// </summary>
    public interface IComponentAdapter
    {
        /// <summary>
        /// Asks ControlAdapter properties on this ComponentAdapter to fill in their fields on the website.
        /// </summary>
        /// <param name="driver">The IWebDriver used to drive the actions</param>
        void PopulateFields(IAutomationSession session);

        /// <summary>
        /// Moves data from the dataModel to the ComponentAdapter
        /// </summary>
        /// <param name="dataModel">The ComponentAdapterModel to use as a data source</param>
        void PopulateFromDataModel(object dataModel);

        /// <summary>
        /// The IWebDriver to associate with this adapter
        /// </summary>
        IAutomationSession Session { get; }

        string GetNameOf(IInputControlAdapter inputControlAdapter);
        Dictionary<string, IInputControlAdapter> GetControlAdapterInfo();

        /// <summary>
        /// Populates the fields on the adapter from the field values of a datasource with matching names.
        /// </summary>
        /// <param name="datagram"></param>
        void PopulateFromDatagram(Dictionary<string, string> datagram);
    }
}
