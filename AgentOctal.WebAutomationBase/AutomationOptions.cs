﻿using System;
using AgentOctal.WebAutomationBase.Navigation;
using Environment = AgentOctal.WebAutomationBase.Navigation.Environment;

namespace AgentOctal.WebAutomationBase
{
    public class AutomationOptions
    {

        public static AutomationOptions Default => new AutomationOptions
        {
            DefaultWaitTimeout = TimeSpan.FromSeconds(30),
            ImplicitWait = TimeSpan.FromMilliseconds(100)
        };

        public string Host;

        public Environment Environment;

        public bool AutoClose;

        public string CertificateName;

        public bool AutoOpen;

        public IUrlBuilder UrlBuilder;

        public bool IgnoreIeZoneRequirements;

        public TimeSpan DefaultWaitTimeout;

        public TimeSpan ImplicitWait;
    }
}
