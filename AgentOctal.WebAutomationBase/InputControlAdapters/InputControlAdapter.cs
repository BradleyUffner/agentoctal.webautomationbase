﻿using System.ComponentModel;
using AgentOctal.WebAutomationBase.ComponentAdapters;
using AgentOctal.WebAutomationBase.Extensions;

namespace AgentOctal.WebAutomationBase.InputControlAdapters
{
    using System;
    using System.Threading;
    using OpenQA.Selenium;

    /// <summary>
    /// Contains logic for adapting an HTML based control to a strongly typed API
    /// </summary>
    /// <typeparam name="T">The type of value this adapter adapts</typeparam>
    public abstract class InputControlAdapter<T> : IInputControlAdapter
    {
        ///// <summary>
        ///// Creates a new instance of an InputControlAdapter
        ///// </summary>
        ///// <param name="locator">A Locator that locates the main HTML content for the control this adapter adapts</param>
        //public InputControlAdapter(Locator locator) : this(locator, null){}

        /// <summary>
        /// Creates a new instance of an InputControlAdapter
        /// </summary>
        /// <param name="locator">A Locator that locates the main HTML content for the control this adapter adapts</param>
        /// <param name="component"></param>
        public InputControlAdapter(By locator, IComponentAdapter component)
        {
            Locator = locator;
            Component = component;
        }

        /// <inheritdoc />
        public IComponentAdapter Component { get; }

        /// <summary>
        /// Returns the Type that the Value property supports
        /// </summary>
        public Type FieldType => typeof(T);

        /// <inheritdoc />
        public string GetName()
        {
            return Component.GetNameOf(this);
        }

        /// <summary>
        /// The value held locator this adapter
        /// </summary>
        public T Value { get; set; }

        /// <summary>
        /// A Locator that locates the main HTML content for the control this adapter adapts
        /// </summary>
        public By Locator { get; }

        /// <summary>
        /// Gets the main WebElement associated with this ControlAdapter
        /// </summary>
        /// <param name="session"></param>
        /// <returns></returns>
        protected IWebElement GetElement(IAutomationSession session) => session.FindElement(Locator);

        /// <summary>
        /// returns true if this adapter contains a value
        /// </summary>
        public bool HasValue => Value != null;

        /// <summary>
        /// Gets or sets the value of this adapter
        /// </summary>
        object IInputControlAdapter.Value
        {
            get => Value;
            set
            {
                if (value is T typedValue) // If the Type of the value already matches the Type (T) of this adapter, set the value
                {
                    Value = typedValue;
                }
                else // If it doesn't try to locate a TypeConverter that can convert the type
                {
                    var converter = TypeDescriptor.GetConverter(typeof(T));
                    if (converter.CanConvertFrom(typeof(string)))
                    {
                        Value = (T)converter.ConvertFrom(value);
                    }
                    else
                    {
                        throw new ArgumentException($"Unable to cast `{value}` to type `{typeof(T)}`");
                    }
                }
            }
        }

        /// <summary>
        /// Applies the adapter's value to the HTML control
        /// </summary>
        /// <param name="session">The AutomationSession</param>
        public void PopulateControl(IAutomationSession session)
        {
            session.Log($"Setting value of `{Component.GetNameOf(this)}` on `{Component.GetType().Name}` to `{Value}`.");
            PopulateControlInternal(session);
        }

        /// <summary>
        /// Applies the adapter's value to the HTML control
        /// </summary>
        /// <param name="session">The automationSession</param>
        public abstract void PopulateControlInternal(IAutomationSession session);

        /// <summary>
        /// Applies the adapter's value to the HTML control
        /// </summary>
        /// <param name="value">The value to enter in to the control</param>
        public void PopulateControl(T value)
        {
            if (Component == null)
            {
                throw new InvalidOperationException(@"The specified ControlAdapter does not have an associated ComponentAdapter.  
If you want to use PopulateControl on an unbound ControlAdapter, you must use the overload that accepts an IWebDriver instance.");
            }

            Value = value;
            PopulateControl(Component.Session);
        }

        /// <summary>
        /// Sleeps the current Thread for a period of time
        /// </summary>
        /// <param name="milliseconds">The number of milliseconds to sleep for</param>
        public static void Sleep(int milliseconds) => Thread.Sleep(milliseconds);

        /// <summary>
        /// Sleeps the current Thread for a period of time
        /// </summary>
        /// <param name="duration">The TimeSpan to sleep for</param>
        public static void Sleep(TimeSpan duration) => Sleep((int)duration.TotalMilliseconds);
    }
}
