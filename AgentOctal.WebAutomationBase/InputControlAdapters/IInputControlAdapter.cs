﻿using System;
using AgentOctal.WebAutomationBase.ComponentAdapters;

namespace AgentOctal.WebAutomationBase.InputControlAdapters
{
    using OpenQA.Selenium;

    public interface IInputControlAdapter
    {
        /// <summary>
        /// Does this field contain a value
        /// </summary>
        bool HasValue { get; }

        /// <summary>
        /// The locator for the field this adapter manages
        /// </summary>
        By Locator { get; }

        /// <summary>
        /// The value of this adapter will set
        /// </summary>
        object Value { get; set; }

        /// <summary>
        /// Takes the current Value property of this adapter, and sets it on the managed field
        /// </summary>
        /// <param name="session"></param>
        void PopulateControl(IAutomationSession session);

        /// <summary>
        /// The ComponentAdapter that owns this ControlAdapter
        /// </summary>
        IComponentAdapter Component { get; }

        /// <summary>
        /// Gets the Type of the value this adapter supports
        /// </summary>
        Type FieldType { get; }

        /// <summary>
        /// Gets the name of this ControlAdapter from its ComponentAdapter
        /// </summary>
        /// <returns></returns>
        string GetName();
    }
}
