﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;

//namespace AgentOctal.WebAutomationBase
//{
//    using System.Diagnostics;
//    using System.Threading;
//    using OpenQA.Selenium;
//    using OpenQA.Selenium.Support.UI;

//    /// <summary>
//    /// A class containing extension methods for IWebDriver
//    /// </summary>
//    public static class IWebDriverExtensions
//    {
//        /// <summary>
//        /// This method allows the caller to poll the browser at defined intervals searching for a WebElement.
//        /// If the element is not found in the timeout window the method throws an exception.
//        /// </summary>
//        /// <param name="driver">The calling instance</param>
//        /// <param name="by">Search pattern</param>
//        /// <param name="timeoutInSeconds">Maximum search length in seconds</param>
//        /// <param name="pollingIntervalInMilliseconds">Frequency of search in milliseconds</param>
//        /// <returns>The target webelement. If the element is not found, an exception is thrown.</returns>
//        public static IWebElement FluentFindElement(this IWebDriver driver, 
//                                                    Locator by,
//                                                    int timeoutInSeconds = 60,
//                                                    int pollingIntervalInMilliseconds = 750)
//        {
//            var waitStrategy = new DefaultWait<IWebDriver>(driver)
//            {
//                Timeout = TimeSpan.FromSeconds(timeoutInSeconds),
//                PollingInterval = TimeSpan.FromMilliseconds(pollingIntervalInMilliseconds)
//            };
//            waitStrategy.IgnoreExceptionTypes(typeof(NoSuchElementException));

//            return waitStrategy.Until(x => x.FindElement(by));
//        }

//        /// <summary>
//        /// This method attempts to compensate for some of the inconsistencies between code triggered UI event generation and physical
//        /// device triggered UI event generation by re-clicking an element up to 5 times until a second expected element is on the screen.
//        /// </summary>
//        /// <param name="driver">Selenuim driver</param>
//        /// <param name="clickElement">Strategy for locating the element to be clicked</param>
//        /// <param name="triggerElement">Strategy for locating the element that is expected to appear after the click</param>
//        /// <param name="clickTimeoutSeconds">Maximum amount of time before returning false</param>
//        /// <param name="clickWaitSeconds">Click interval during the timeout window</param>
//        /// <returns>A boolean indicating the success of clicking the control and seeing the expected element appear</returns>
//        public static bool ClickUntil(this IWebDriver driver,
//                                      Locator clickElement,
//                                      Locator triggerElement, 
//                                      int clickTimeoutSeconds = 60,
//                                      int clickWaitSeconds = 2)
//        {
//            var abortTime = DateTime.Now.AddSeconds(clickTimeoutSeconds);

//            while (DateTime.Now < abortTime)
//            {

//                IWebElement element = driver.FluentFindElement(clickElement);
//                element.Click();

//                Thread.Sleep(1000 * clickWaitSeconds);

//                try
//                {
//                    //Debug.WriteLine($"trying to click on {clickElement.Value} using {clickElement.StrategyType}");
//                    IWebElement locationResult = driver.FindElement(triggerElement);

//                    if (locationResult != null)
//                    {
//                        return true;
//                    }
//                }
//                catch (NoSuchElementException)
//                {
//                    //ignore exception
//                }
//            }

//            return false;
//        }

//        /// <summary>
//        /// This method attempts to compensate for some of the inconsistencies between code triggered UI event generation and physical
//        /// device triggered UI event generation by re-clicking an element while a second element remains on the screen.
//        /// </summary>
//        /// <param name="driver">Selenuim driver</param>
//        /// <param name="clickElement">Strategy for locating the element to be clicked</param>
//        /// <param name="triggerElement">Strategy for locating the element that is expected to disappear after the click</param>
//        /// <param name="findTimeoutSeconds">Maximum amount of time before returning false</param>
//        /// <param name="clickWaitSeconds">Click interval during the timeout window</param>
//        /// <returns>A boolean indicating the success of clicking the control and seeing the expected element disappear</returns>
//        public static bool ClickWhile(this IWebDriver driver,
//                                      Locator clickElement,
//                                      Locator triggerElement,
//                                      int findTimeoutSeconds = 2, 
//                                      int clickWaitSeconds = 2)
//        {
//            int clickFailures = 0;
//            bool clickFailed = true;

//            do
//            {
//                try
//                {
//                    //Debug.WriteLine($"trying to click on {clickElement.Value} using {clickElement.StrategyType}");
//                    driver.FluentFindElement(clickElement).Click();
//                    Thread.Sleep(1000 * clickWaitSeconds);
//                    IWebElement locationResult = driver.FluentFindElement(triggerElement, findTimeoutSeconds);
//                    clickFailed = (locationResult != null);
//                }
//                catch { clickFailed = false; }
//                if (clickFailed) clickFailures++;
//            } while (clickFailures < 5 && clickFailed);

//            return !clickFailed;
//        }

//        /// <summary>
//        /// This method attempts to compensate for some of the inconsistencies between code triggered UI event generation and physical
//        /// device triggered UI event generation by re-clicking an element while that element remains on the screen. Commonly for
//        /// buttons that disappear when submit is successful
//        /// </summary>
//        /// <param name="driver">Selenuim driver</param>
//        /// <param name="clickElement">Strategy for locating the element to be clicked</param>
//        /// <param name="findTimeoutSeconds">Maximum amount of time before returning false</param>
//        /// <param name="clickWaitSeconds">Click interval during the timeout window</param>
//        /// <returns>A boolean indicating the success of clicking the control and seeing that control disappear</returns>
//        public static bool ClickWhile(this IWebDriver driver,
//                                      Locator clickElement,
//                                      int findTimeoutSeconds = 2,
//                                      int clickWaitSeconds = 2)
//        {
//            return ClickWhile(driver,
//                              clickElement,
//                              clickElement,
//                              findTimeoutSeconds,
//                              clickWaitSeconds);
//        }
//    }
//}
