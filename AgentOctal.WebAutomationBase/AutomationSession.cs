﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Text;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using AgentOctal.WebAutomationBase.Logging;
using AgentOctal.WebAutomationBase.Navigation;
using OpenQA.Selenium;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Support.UI;
using SeleniumExtras.WaitHelpers;
using LogLevel = AgentOctal.WebAutomationBase.Logging.LogLevel;

namespace AgentOctal.WebAutomationBase
{
    public interface IAutomationSession
    {
        event EventHandler<ScreenshotEventArgs> LoggedScreenshot;
        event EventHandler<MessageEventArgs> LoggedMessage;
        event EventHandler<ExceptionEventArgs> LoggedException;
        event EventHandler<WaitingEventArgs> Waiting;
        event EventHandler<EventArgs> WaitCompleted;

        AreaManager AreaManager { get; }
        IWebDriver Driver { get; }
        AutomationOptions Options { get; }
        void NavigateTo(Area area);
        void NavigateTo(string url);
        void Dispose();
        Bitmap TakeScreenshot(IWebElement element);
        Bitmap TakeScreenshot();
        void Open(TimeSpan timeout);
        void Close();
        void Log(string message);
        void Log(Bitmap screenshot, string description);
        void Log(Exception ex);
        void Log(string message, LogLevel logLevel);
        void Log(Bitmap screenshot, string description, LogLevel logLevel);
        void Log(Exception ex, LogLevel logLevel);

        /// <summary>
        /// Logs a screenshot of the entire browser window.
        /// </summary>
        void LogScreenshot();

        /// <summary>
        /// Logs a screenshot of the entire browser window.
        /// </summary>
        /// <param name="description">A description of the screenshot</param>
        void LogScreenshot(string description);

        /// <summary>
        /// Logs a screenshot of the specified element.
        /// </summary>
        /// <param name="element">The element to take a screenshot of</param>
        void LogScreenshot(IWebElement element);

        /// <summary>
        /// Logs a screenshot of the specified element.
        /// </summary>
        /// <param name="element">The element to take a screenshot of</param>
        /// <param name="description">A description of the screenshot</param>
        void LogScreenshot(IWebElement element, string description);

        void Sleep(TimeSpan duration);
        void OnWaiting(string @for, TimeSpan timeout);
        void OnWaitCompleted();
    }

    public class AutomationSession<TAutomationOptions> :
        IAutomationSession,
        IDisposable
        where TAutomationOptions : AutomationOptions
    {
        public event EventHandler<ScreenshotEventArgs> LoggedScreenshot;
        public event EventHandler<MessageEventArgs> LoggedMessage;
        public event EventHandler<ExceptionEventArgs> LoggedException;
        public event EventHandler<WaitingEventArgs> Waiting;
        public event EventHandler<EventArgs> WaitCompleted;

        public static AutomationSession<TAutomationOptions> Begin(TAutomationOptions options, int timeoutSeconds = 60)
        {
            return new AutomationSession<TAutomationOptions>(options, TimeSpan.FromSeconds(timeoutSeconds));
        }


        protected AutomationSession(TAutomationOptions options, TimeSpan timeout)
        {
            AreaManager = new AreaManager();

            Options = options;

            if (options.AutoOpen)
            {
                Open(timeout);
            }
        }
        public AreaManager AreaManager { get; protected set; }

        public void NavigateTo(Area area)
        {
            if (area is UriArea)
            {
                NavigateTo(area.Path);
            }
            else
            {
                var url = Options.UrlBuilder.GetUrlFor(this, area);
                Log($"Navigating to `{url}`");
                OnWaiting($"Navigating to {url}", TimeSpan.FromMinutes(5));
                Driver.Navigate().GoToUrl(url);
                OnWaitCompleted();
            }

        }
        public void NavigateTo(string url)
        {
            Log($"Navigating to `{url}`");
            OnWaiting($"Navigating to {url}", TimeSpan.FromMinutes(5));
            Driver.Navigate().GoToUrl(url);
            OnWaitCompleted();
        }

        public void Open(TimeSpan timeout)
        {
            Log("Opening web browser");
            Driver = InstantiateAndConfigureWebDriver(timeout);
            AfterOpen(timeout);
        }

        protected virtual void AfterOpen(TimeSpan timeout) { }

        public IWebDriver Driver { get; private set; }

        public TAutomationOptions Options { get; }

        AutomationOptions IAutomationSession.Options => Options;

        public virtual void Close()
        {
            try
            {
                if (Driver != null)
                {
                    Driver.Close();
                    Driver.Quit();
                }
                Shutdown();
            }
            catch
            {
                //Ignore errors related to closing the browser window
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            if (!disposing) return;
            if (Options.AutoClose)
            {
                Close();
            }
        }

        protected virtual void Shutdown()
        {
            if (Service != null && Service.IsRunning)
            {
                var driverService = System.Diagnostics.Process.GetProcessById(Service.ProcessId);
                driverService.Kill();
            }
        }

        public InternetExplorerDriverService Service
        {
            get; private set;
        }

        /// <summary>
        /// This method returns an Internet Explorer Selenium Web Driver configured to implicitly wait 
        /// 1/4 second before executing any command
        /// </summary>
        /// <returns>Internet Explorer Selenium Web Driver instance</returns>
        protected virtual IWebDriver InstantiateAndConfigureWebDriver(TimeSpan timeout)
        {
            var driverOpts = new InternetExplorerOptions();
            if (Options.IgnoreIeZoneRequirements)
            {
                driverOpts.IntroduceInstabilityByIgnoringProtectedModeSettings = true;
            }

            Service = InternetExplorerDriverService.CreateDefaultService();
            Service.HideCommandPromptWindow = true;
            IWebDriver driver = new InternetExplorerDriver(Service, driverOpts);
            driver.Manage().Timeouts().ImplicitWait = Options.ImplicitWait;

            return driver;
        }

        public Bitmap TakeScreenshot()
        {
            return TakeScreenshot(null);
        }

        public Bitmap TakeScreenshot(IWebElement element)
        {
            var camera = ((ITakesScreenshot)Driver).GetScreenshot();
            using (var s = new MemoryStream(camera.AsByteArray))
            {
                var screenshot = (Bitmap)Image.FromStream(s);
                if (element == null) //If no element specified, use the entire screenshot
                {
                    return screenshot;
                }
                else //Element was specified, so crop the screenshot to the bounds of the element
                {
                    var croppedImage = new Bitmap(element.Size.Width, element.Size.Height, PixelFormat.Format32bppArgb);
                    using (var g = Graphics.FromImage(croppedImage))
                    {
                        g.DrawImage(image: screenshot,
                                    destRect: new Rectangle(0, 0, croppedImage.Width, croppedImage.Height),
                                    srcRect: new Rectangle(element.Location, element.Size),
                                    srcUnit: GraphicsUnit.Pixel);
                        screenshot.Dispose(); // get rid of the original screenshot since we don't need it any more
                    }

                    return croppedImage;
                }

            }
        }

        protected virtual void OnLoggedScreenshot(ScreenshotEventArgs e)
        {
            LoggedScreenshot?.Invoke(this, e);
        }

        protected virtual void OnLoggedMessage(MessageEventArgs e)
        {
            LoggedMessage?.Invoke(this, e);
        }

        protected virtual void OnLoggedException(ExceptionEventArgs e)
        {
            LoggedException?.Invoke(this, e);
        }

        public virtual void OnWaiting(string @for, TimeSpan timeout)
        {
            Waiting?.Invoke(this, new WaitingEventArgs(@for, timeout));
        }
        public virtual void OnWaitCompleted()
        {
            WaitCompleted?.Invoke(this, EventArgs.Empty);
        }

        public void Log(string message)
        {
            OnLoggedMessage(new MessageEventArgs(message));
        }

        public void Log(Bitmap screenshot, string description)
        {
            OnLoggedScreenshot(new ScreenshotEventArgs(screenshot, description));
        }

        public void Log(Exception ex)
        {
            OnLoggedException(new ExceptionEventArgs(ex, LogLevel.High));
        }



        public void Log(string message, LogLevel logLevel)
        {
            OnLoggedMessage(new MessageEventArgs(message, logLevel));
        }

        public void Log(Bitmap screenshot, string description, LogLevel logLevel)
        {
            OnLoggedScreenshot(new ScreenshotEventArgs(screenshot, description, logLevel));
        }

        public void Log(Exception ex, LogLevel logLevel)
        {
            OnLoggedException(new ExceptionEventArgs(ex, logLevel));
        }

        /// <summary>
        /// Logs a screenshot of the entire browser window.
        /// </summary>
        public void LogScreenshot()
        {
            LogScreenshot(null, "(no description");
        }

        /// <summary>
        /// Logs a screenshot of the entire browser window.
        /// </summary>
        /// <param name="description">A description of the screenshot</param>
        public void LogScreenshot(string description)
        {
            LogScreenshot(null, description);
        }

        /// <summary>
        /// Logs a screenshot of the specified element.
        /// </summary>
        /// <param name="element">The element to take a screenshot of</param>
        public void LogScreenshot(IWebElement element)
        {
            LogScreenshot(element, "(no description");
        }

        /// <summary>
        /// Logs a screenshot of the specified element.
        /// </summary>
        /// <param name="element">The element to take a screenshot of</param>
        /// <param name="description">A description of the screenshot</param>
        public void LogScreenshot(IWebElement element, string description)
        {
            Log(TakeScreenshot(element), description);
        }

        public void Sleep(TimeSpan duration)
        {
            OnWaiting("Pausing", duration);
            Thread.Sleep(duration);
            OnWaitCompleted();
        }
    }
}
